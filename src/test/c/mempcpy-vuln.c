#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef __aarch64__
#define STACK_DEPTH 24
#endif
#ifdef __x86_64__
#define STACK_DEPTH 40
#endif

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
#pragma GCC push_options
#pragma GCC optimize ("O0") // O1 optimises out the memcpy calls!
void mempcpy_vuln(){
  char buf1[16];
  void *mbuf = malloc(STACK_DEPTH * 2);
  memset(buf1, 0, 16);
  memset(mbuf, 0, STACK_DEPTH * 2);
  fgets(mbuf, STACK_DEPTH * 2, stdin); // no error, just here to activate the input for memcpy optimisations
  puts(mbuf);
  mempcpy(buf1, mbuf, sizeof(buf1)); // no error
  puts(buf1);
  mempcpy(buf1, mbuf, STACK_DEPTH + 1); // partial smash
  puts(buf1);
  mempcpy(buf1, mbuf, STACK_DEPTH + 8); // complete smash
  puts(buf1);
  free(mbuf);
}
#endif