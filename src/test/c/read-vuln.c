#if _FORTIFY_SOURCE > 0
  #undef _FORTIFY_SOURCE
  #define _FORTIFY_SOURCE 0
#endif

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#ifdef __aarch64__
#define STACK_DEPTH 32
#endif
#ifdef __x86_64__
#define STACK_DEPTH 56
#endif

#ifndef STACK_DEPTH
#error STACK_DEPTH was not defined for this architecture.
#else
void read_vuln(){
  char __attribute__((no_reorder)) buf2[16];
  char __attribute__((no_reorder)) buf1[16];
  memset(buf1, 0, 16);
  memset(buf2, 0, 16);
  read(STDIN_FILENO, buf1, sizeof(buf1)); // no error
  puts(buf1);
  read(STDIN_FILENO, buf1, sizeof(buf1) + 1); // overrun
  puts(buf1);
  read(STDIN_FILENO, buf1, STACK_DEPTH + 1); // partial smash
  puts(buf1);
  read(STDIN_FILENO, buf1, STACK_DEPTH + 8); // complete smash
  puts(buf1);
  read(STDIN_FILENO, buf2, STACK_DEPTH - sizeof(buf2) + 1); // partial smash
  puts(buf2);
  read(STDIN_FILENO, buf2, STACK_DEPTH - sizeof(buf2) + 8); // complete smash
  puts(buf2);
}
#endif
