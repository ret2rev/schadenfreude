package com.tamuctf.tools.ghidra.schadenfreude.analyzers;

import com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite;
import ghidra.util.classfinder.ClassSearcher;
import org.junit.jupiter.api.BeforeAll;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public abstract class AnalyzerTest {

    @BeforeAll
    public static void confirmLoaded() {
        assertNotNull(SchadenfreudeTestSuite.getPrograms());
        List<Class<? extends BufferFixupAnalyzer>> classes = ClassSearcher.getClasses(BufferFixupAnalyzer.class);
        assertEquals(1, classes.size());
    }

}
