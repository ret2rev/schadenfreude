package com.tamuctf.tools.ghidra.schadenfreude.test.application;

import ghidra.framework.ApplicationConfiguration;
import ghidra.framework.ModuleInitializer;
import ghidra.util.Msg;
import ghidra.util.classfinder.ClassSearcher;
import ghidra.util.exception.CancelledException;

import java.util.List;

public class SchadenfreudeApplicationConfiguration extends ApplicationConfiguration {

    @Override
    protected void initializeApplication() {
        super.initializeApplication();

        monitor.setMessage("Performing class searching...");
        performClassSearching();

        monitor.setMessage("Performing module initialization...");
        performModuleInitialization();

        monitor.setMessage("Done initializing");
    }

    private void performClassSearching() {
        // The class searcher searches the classpath, and Ghidra's classpath should be complete
        // for this configuration at this point.
        try {
            ClassSearcher.search(true, monitor);
        } catch (CancelledException e) {
            Msg.debug(this, "Class searching unexpectedly cancelled.");
        }
    }

    private void performModuleInitialization() {
        List<ModuleInitializer> instances = ClassSearcher.getInstances(ModuleInitializer.class);
        for (ModuleInitializer initializer : instances) {
            monitor.setMessage("Initializing " + initializer.getName() + "...");
            initializer.run();
        }
    }

}
