package com.tamuctf.tools.ghidra.schadenfreude.analyzers;

import com.tamuctf.tools.ghidra.schadenfreude.test.util.FunctionUtil;
import ghidra.program.model.data.Array;
import ghidra.program.model.data.CharDataType;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.program.model.listing.Variable;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.params.ParameterizedTest.DEFAULT_DISPLAY_NAME;

public class BufferFixupTest extends AnalyzerTest {

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesGetsVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "gets_vuln", 1, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesFgetsVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "fgets_vuln", 2, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesFgetsVulnCaller(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "fgets_vuln_caller", 2, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesFreadVuln(@NotNull Program program) {
        // fread actually takes a void*, so we can't determine its base type as easily.
        // In future iterations, perhaps we could make it expect specific undefineds due to the input size?
        // Note that an invocation to `puts` fixes this
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "fread_vuln", 2, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesMemcpyVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "memcpy_vuln", 1, 24);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesReadVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "read_vuln", 2, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    public void fixesMempcpyVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "mempcpy_vuln", 1, 16);
    }

    @ParameterizedTest(name = "{displayName}" + DEFAULT_DISPLAY_NAME)
    @MethodSource("com.tamuctf.tools.ghidra.schadenfreude.SchadenfreudeTestSuite#getPrograms")
    @Disabled("Somehow doesn't work after 9.2 update; investigating.")
    public void fixesMemccpyVuln(@NotNull Program program) {
        fixesBuffer(program, a -> CharDataType.dataType.isEquivalent(a.getDataType()), "memccpy_vuln", 1, 24);
    }


    private void fixesBuffer(@NotNull Program program, @NotNull Predicate<Array> typeCheck, @NotNull String name,
            int count, int size) {
        Function target = FunctionUtil.getFunctionByName(program, name);
        assertNotNull(target);
        assertEquals(count,
                Arrays.stream(target.getAllVariables()).filter(Variable::hasStackStorage).map(Variable::getDataType)
                        .filter(Array.class::isInstance).map(Array.class::cast).filter(typeCheck)
                        .filter(dt -> dt.getLength() == size).count(),
                () -> "Actually found: " + Arrays.toString(target.getAllVariables()));
    }

}
