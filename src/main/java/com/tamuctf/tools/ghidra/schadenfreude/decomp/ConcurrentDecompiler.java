package com.tamuctf.tools.ghidra.schadenfreude.decomp;

import ghidra.app.decompiler.DecompInterface;
import ghidra.app.decompiler.DecompileOptions;
import ghidra.app.decompiler.DecompileResults;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.util.task.TaskMonitor;

import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class ConcurrentDecompiler {

    private final ConcurrentHashMap<Function, DecompileResults> cache;
    private final Queue<DecompInterface> decompilers;
    private final Semaphore available;
    private final DecompileOptions decOptions;
    private final Program program;
    @SuppressWarnings("FieldCanBeLocal") private final Timer killer;
    private final int currThreads;

    public ConcurrentDecompiler(int timeout, int threads, DecompileOptions decOptions, Program program) {
        this.cache = new ConcurrentHashMap<>();
        this.decompilers = new ConcurrentLinkedQueue<>();
        this.available = new Semaphore(threads);
        this.decOptions = decOptions;
        this.program = program;
        this.killer = new Timer(true);
        this.currThreads = threads;
        killer.schedule(new KillerTask(), timeout, timeout);
    }

    public DecompileResults decompileFunction(Function function, int timeout, TaskMonitor monitor, boolean refresh) {
        return cache.compute(function, (f, existing) -> {
            if (!refresh && existing != null && existing.decompileCompleted()) {
                return existing;
            }
            try {
                return decompile(di -> di.decompileFunction(f, timeout, monitor));
            } catch (InterruptedException e) {
                return null;
            }
        });
    }

    private DecompileResults decompile(
            java.util.function.Function<DecompInterface, DecompileResults> consumer) throws InterruptedException {
        available.acquire();
        DecompInterface iface = decompilers.poll();
        if (iface == null) {
            iface = new DecompInterface();
            iface.setOptions(decOptions);
            iface.toggleCCode(false);
            iface.toggleSyntaxTree(true);
            iface.openProgram(program);
        }
        try {
            DecompileResults results = consumer.apply(iface);
            while (results.failedToStart()) {
                iface.dispose();
                iface = new DecompInterface();
                iface.setOptions(decOptions);
                iface.toggleCCode(false);
                iface.toggleSyntaxTree(true);
                iface.openProgram(program);
                results = consumer.apply(iface);
            }
            return results;
        } finally {
            decompilers.add(iface);
            available.release();
        }
    }

    public synchronized void resize(int threads) throws InterruptedException {
        int diff = threads - currThreads;
        if (diff > 0) {
            available.release(diff);
        } else if (diff < 0) {
            diff = -diff;
            available.acquire(diff);
            for (int i = 0; i < diff; ++i) {
                DecompInterface decomp = decompilers.poll();
                if (decomp != null) {
                    decomp.dispose();
                }
            }
        }
    }

    public synchronized void dispose() throws InterruptedException {
        available.acquire(currThreads);
        while (!decompilers.isEmpty()) {
            decompilers.poll().dispose();
        }
    }

    public void flushCache() {
        this.cache.clear();
    }

    private class KillerTask extends TimerTask {

        @Override
        public void run() {
            int inactive = available.drainPermits();
            for (int i = 0; i < inactive; ++i) {
                DecompInterface existing = decompilers.poll(); // huzzah, thread safety
                if (existing != null) {
                    existing.dispose();
                }
            }
            available.release(inactive);
        }

    }

}
