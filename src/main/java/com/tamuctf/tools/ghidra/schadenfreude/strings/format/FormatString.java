package com.tamuctf.tools.ghidra.schadenfreude.strings.format;

import lombok.Value;
import lombok.experimental.NonFinal;

@Value
@NonFinal
public class FormatString<FS extends FormatStringEntry<T>, T> {

    CharSequence unprocessed;
    FS[] entries;

}
