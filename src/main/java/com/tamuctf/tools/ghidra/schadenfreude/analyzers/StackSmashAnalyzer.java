/* ###
 * IP: GHIDRA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tamuctf.tools.ghidra.schadenfreude.analyzers;

import com.tamuctf.tools.ghidra.schadenfreude.decomp.SchadenfreudeDecompiler;
import com.tamuctf.tools.ghidra.schadenfreude.util.ProgramUtils;
import com.tamuctf.tools.ghidra.schadenfreude.util.TaskMonitorWrapper;
import ghidra.app.services.AbstractAnalyzer;
import ghidra.app.services.AnalysisPriority;
import ghidra.app.services.AnalyzerType;
import ghidra.app.util.importer.MessageLog;
import ghidra.framework.options.Options;
import ghidra.program.model.address.Address;
import ghidra.program.model.address.AddressSetView;
import ghidra.program.model.data.PointerDataType;
import ghidra.program.model.lang.Register;
import ghidra.program.model.listing.*;
import ghidra.program.model.pcode.*;
import ghidra.util.task.CancelledListener;
import ghidra.util.task.TaskMonitor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * TODO: Provide class-level documentation that describes what this analyzer
 * does.
 */
public class StackSmashAnalyzer extends AbstractAnalyzer {

    public static final AnalysisPriority PRIORITY = BufferFixupAnalyzer.PRIORITY.after();
    private static final String ANALYZER_NAME = "Stack Smasher";
    private static final String ANALYZER_DESCRIPTION = "A crude analysis for stack smashers";
    private static final String TRAVERSE_OPTION = "Traverse function calls?";
    private static final boolean TRAVERSE_DEFAULT = true;
    private static final String TRAVERSE_DESCRIPTION = "Traverse through function calls to determine types more precisely based on how they're used";
    private static final String TIMEOUT_OPTION = "Decompiler Timeout";
    private static final int TIMEOUT_DEFAULT = 3;
    private static final String TIMEOUT_DESCRIPTION = "Number of seconds before the decompiler times out";
    private static final List<String> LIBC_NAMES = List.of("libc.so.6");
    private final SchadenfreudeDecompiler decomp;
    private boolean traverse;
    private int timeout;

    public StackSmashAnalyzer() {
        super(ANALYZER_NAME, ANALYZER_DESCRIPTION, AnalyzerType.FUNCTION_ANALYZER);
        this.setSupportsOneTimeAnalysis();
        this.setPriority(PRIORITY);
        traverse = TRAVERSE_DEFAULT;
        timeout = TIMEOUT_DEFAULT;
        decomp = SchadenfreudeDecompiler.getInstance();
    }

    @Override
    public boolean getDefaultEnablement(Program program) {
        return StreamSupport.stream(program.getFunctionManager().getExternalFunctions().spliterator(), false)
                .noneMatch(f -> f.getName().equals("__stack_chk_fail"));
    }

    @Override
    public boolean canAnalyze(Program program) {
        return program.getLanguage().supportsPcode() &&
                Arrays.stream(program.getExternalManager().getExternalLibraryNames()).anyMatch(LIBC_NAMES::contains);
    }

    @Override
    public void optionsChanged(Options options, Program program) {
        timeout = options.getInt(TIMEOUT_OPTION, TIMEOUT_DEFAULT);
        traverse = options.getBoolean(TRAVERSE_OPTION, TRAVERSE_DEFAULT);
    }

    @Override
    public void registerOptions(Options options, Program program) {
        options.registerOption(TIMEOUT_OPTION, TIMEOUT_DEFAULT, null, TIMEOUT_DESCRIPTION);
        options.registerOption(TRAVERSE_OPTION, TRAVERSE_DEFAULT, null, TRAVERSE_DESCRIPTION);
    }

    private Map<HighFunction, List<PcodeOp>> decompileFunctions(Collection<Function> functions, TaskMonitor monitor) {
        CountDownLatch latch = new CountDownLatch(functions.size());

        monitor.initialize(functions.size());
        monitor.setIndeterminate(false);
        monitor.setMessage("Decompiling " + functions.size() + " functions");

        Map<HighFunction, List<PcodeOp>> results = new ConcurrentHashMap<>();
        for (Function function : functions) {
            decomp.decompileFunction(function, timeout, monitor, res -> {
                if (res.decompileCompleted()) {
                    LinkedList<PcodeOp> ops = new LinkedList<>();
                    HighFunction high = res.getHighFunction();
                    high.getPcodeOps().forEachRemaining(ops::add);
                    results.put(high, ops);
                }
                monitor.incrementProgress(1);
                latch.countDown();
            });
        }

        Thread currentThread = Thread.currentThread();
        CancelledListener listener = currentThread::interrupt;
        try {
            monitor.addCancelledListener(listener);
            latch.await();
        } catch (InterruptedException e) {
            return null;
        } finally {
            monitor.removeCancelledListener(listener);
        }

        return results;
    }

    @Override
    public boolean added(Program program, AddressSetView set, TaskMonitor monitor, MessageLog log) {
        decomp.openProgram(program);
        decomp.flushCaches();

        if (StreamSupport.stream(program.getFunctionManager().getExternalFunctions().spliterator(), false)
                .anyMatch(f -> f.getName().equals("__stack_chk_fail"))) {
            log.appendMsg(
                    "__stack_chk_fail is defined; stacks can only be smashed if a canary disclosure bug is present.");
        }

        Register stackRegister = ProgramUtils.getStackRegister(program);

        List<Function> functions = StreamSupport
                .stream(program.getFunctionManager().getFunctions(set, true).spliterator(), false)
                .filter(f -> !f.isThunk()) // no point
                .collect(Collectors.toUnmodifiableList());

        if (functions.isEmpty()) {
            return false;
        }

        monitor.initialize(functions.size());

        Map<HighFunction, List<PcodeOp>> pcodes = decompileFunctions(functions, monitor);
        if (pcodes == null) {
            return false;
        }

        monitor.setIndeterminate(true);
        monitor.initialize(1);
        TaskMonitor wrapped = new TaskMonitorWrapper(monitor);

        List<SmashResult> results = pcodes.values().stream().flatMap(Collection::stream)
                .filter(op -> op.getOpcode() == PcodeOp.CALL)
                .peek(op -> monitor.setMessage("Attempting to smash " + op.getSeqnum().getTarget() + "..."))
                .flatMap(op -> smashCall(program, op, stackRegister, wrapped).stream()).filter(Objects::nonNull)
                .collect(Collectors.toUnmodifiableList());

        monitor.incrementProgress(1);

        results.stream().filter(SmashResult::isOverrun).forEach(res -> {
            String varComment = res.getSmashed().getComment();
            String varStatement = res.getVarStatement();
            if (varComment == null || varComment.isBlank()) {
                res.getSmashed().setComment(varStatement);
            } else if (!varComment.contains(varStatement)) {
                res.getSmashed().setComment(String.format("%s\n%s", varComment, varStatement));
            }
            Instruction inst = program.getListing().getInstructionAt(res.getWhere());
            String instComment = inst.getComment(CodeUnit.PRE_COMMENT);
            String instStatement = res.getInstStatement();
            if (instComment == null || instComment.isBlank()) {
                inst.setComment(CodeUnit.PRE_COMMENT, instStatement);
            } else if (!instComment.contains(instStatement)) {
                inst.setComment(CodeUnit.PRE_COMMENT, String.format("%s\n%s", instComment, instStatement));
            }
        });

        return true;
    }

    private List<SmashResult> smashCall(Program program, PcodeOp op, Register stackRegister, TaskMonitor monitor) {
        Function smasher = program.getFunctionManager().getFunctionContaining(op.getInput(0).getAddress());
        switch (smasher.getName()) {
            case "gets":
                return getsSmash(smasher, program, op, stackRegister, monitor);
            case "fgets":
                return fgetsSmash(smasher, program, op, stackRegister, monitor);
            case "fread":
                return freadSmash(smasher, program, op, stackRegister, monitor);
            case "memcpy":
            case "mempcpy":
                return memcpySmash(smasher, program, op, stackRegister, monitor);
            case "memccpy":
                return memccpySmash(smasher, program, op, stackRegister, monitor);
            case "read":
                return readSmash(smasher, program, op, stackRegister, monitor);
            case "scanf":
                return scanfSmash(smasher, program, op, stackRegister, monitor);
            default:
                return Collections.emptyList();
        }
    }

    private List<SmashResult> memcpySmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        return getRootStackDefinitions(program, op.getInput(1), stackRegister, monitor).stream().flatMap(
                target -> getSizedSmashStream(smasher, op, target, deriveSizes(program, op.getInput(3), monitor)))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<SmashResult> memccpySmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        return getRootStackDefinitions(program, op.getInput(1), stackRegister, monitor).stream().flatMap(
                target -> getSizedSmashStream(smasher, op, target, deriveSizes(program, op.getInput(4), monitor)))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<SmashResult> getsSmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        return getRootStackDefinitions(program, op.getInput(1), stackRegister, monitor).stream()
                .map(target -> getUnsizedSmashStream(smasher, op, target)).collect(Collectors.toList());
    }

    private List<SmashResult> fgetsSmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        return getRootStackDefinitions(program, op.getInput(1), stackRegister, monitor).stream().flatMap(
                target -> getSizedSmashStream(smasher, op, target, deriveSizes(program, op.getInput(2), monitor)))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<SmashResult> freadSmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        Set<Optional<Integer>> data_sizes = deriveSizes(program, op.getInput(2), monitor);
        Set<Optional<Integer>> nmemb_sizes = deriveSizes(program, op.getInput(3), monitor);

        Set<Optional<Integer>> sizes = data_sizes.stream().flatMap(i -> nmemb_sizes.stream().map(j -> {
            if (i.isEmpty() || j.isEmpty()) {
                return Optional.<Integer>empty();
            }
            return Optional.of(i.get() * j.get());
        })).collect(Collectors.toSet());
        return getRootStackDefinitions(program, op.getInput(1), stackRegister, monitor).stream()
                .flatMap(target -> getSizedSmashStream(smasher, op, target, sizes)).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<SmashResult> readSmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        return getRootStackDefinitions(program, op.getInput(2), stackRegister, monitor).stream().flatMap(
                target -> getSizedSmashStream(smasher, op, target, deriveSizes(program, op.getInput(3), monitor)))
                .filter(Objects::nonNull).collect(Collectors.toList());
    }

    private List<SmashResult> scanfSmash(Function smasher, Program program, PcodeOp op, Register stackRegister,
            TaskMonitor monitor) {
        // TODO
        return Collections.emptyList();
    }

    private SmashResult getUnsizedSmashStream(Function smasher, PcodeOp op, HighVariable target) {
        SmashInfo info = SmashInfo.get(target);
        if (info == null || !isStack(target)) {
            return null;
        }
        return new SmashResult(smasher, op.getSeqnum().getTarget(), info.getSmashed(),
                String.format("Stack smashed at %s (by %s) (+%d,%d,%d)", op.getSeqnum().getTarget(), smasher,
                        info.getOffset(), info.getEnd(), info.getSmashSize()),
                String.format("Smashes stack using %s@0x%s (+%d,%d,%d)", info.getSmashed().getName(),
                        info.getSmashed().getFunction().getEntryPoint(), info.getOffset(), info.getEnd(),
                        info.getSmashSize()), info.getSmashSize(), true, true, true);
    }

    private Stream<SmashResult> getSizedSmashStream(Function smasher, PcodeOp op, HighVariable target,
            Set<Optional<Integer>> sizes) {
        int pointerSize = smasher.getProgram().getDefaultPointerSize();
        SmashInfo info = SmashInfo.get(target);
        if (info == null || !isStack(target)) {
            return Stream.empty();
        }
        return sizes.stream().map(size -> size.map(i -> {
            if (i > info.getEnd()) {
                boolean complete, log;
                StringBuilder vsBuilder = new StringBuilder();
                StringBuilder psBuilder = new StringBuilder();
                if (i > info.getSmashSize()) {
                    if (i < info.getSmashSize() + pointerSize) {
                        complete = false;
                        vsBuilder.append("Stack partially smashed at ");
                        psBuilder.append("Smashes stack partially using ");
                    } else {
                        complete = true;
                        vsBuilder.append("Stack smashed at ");
                        psBuilder.append("Smashes stack using ");
                    }
                    log = true;
                } else {
                    complete = false;
                    log = false;
                    String affected = String.join(",",
                            (Iterable<String>) () -> Arrays.stream(info.getFrame().getStackVariables())
                                    .sorted(Comparator.comparingInt(Variable::getStackOffset))
                                    .filter(v -> info.getSmashed().getStackOffset() < v.getStackOffset() &&
                                            v.getStackOffset() - info.getSmashed().getStackOffset() < i)
                                    .map(Variable::getName).iterator());
                    if (!affected.isBlank()) {
                        vsBuilder.append("Buffer overrun of ").append(affected).append(" at ");
                        psBuilder.append("Overruns ").append(affected).append(" using ");
                    } else {
                        vsBuilder.append("Buffer overrun at ");
                        psBuilder.append("Overruns using ");
                    }
                }
                vsBuilder.append(op.getSeqnum().getTarget()).append(" (by ").append(smasher).append(", size ").append(i)
                        .append(") (+").append(info.getOffset()).append(',').append(info.getEnd()).append(',')
                        .append(info.getSmashSize()).append(')');
                psBuilder.append(info.getSmashed().getName()).append("(+").append(info.getOffset()).append(',')
                        .append(info.getEnd()).append(',').append(info.getSmashSize()).append(')');
                return new SmashResult(smasher, op.getSeqnum().getTarget(), info.getSmashed(), vsBuilder.toString(),
                        psBuilder.toString(), i, complete, log, true);
            }
            return new SmashResult(smasher, op.getSeqnum().getTarget(), info.getSmashed(), null, null, i, false, false,
                    false);
        }).orElseGet(() -> new SmashResult(smasher, op.getSeqnum().getTarget(),
                info.getFrame().getVariableContaining(target.getSymbol().getStorage().getStackOffset()),
                String.format("Possibly stack smashed at %s (by %s, size ?) (+%d,%d,%d)", op.getSeqnum().getTarget(),
                        smasher, info.getOffset(), info.getEnd(), info.getSmashSize()),
                String.format("Possibly smashes stack using %s (+%d,%d,%d)", info.getSmashed().getName(),
                        info.getOffset(), info.getEnd(), info.getSmashSize()), info.getSmashSize(), false, true,
                true)));
    }

    private Set<Optional<Integer>> deriveSizes(Program program, Varnode node, TaskMonitor monitor) {
        Set<Optional<Integer>> sizes = new HashSet<>();
        if (node.isConstant()) {
            sizes.add(Optional.of((int) node.getOffset()));
        } else {
            Set<Varnode> visited = new HashSet<>();
            Queue<Varnode> queue = new LinkedBlockingQueue<>();
            queue.offer(node);
            while (!queue.isEmpty()) {
                Varnode vn = queue.poll();
                visited.add(vn);
                if (vn.isConstant()) {
                    sizes.add(Optional.of((int) vn.getOffset()));
                } else if (traverse && vn.getHigh() != null && vn.getHigh() instanceof HighParam) {
                    resolveThroughFunction(program, (HighParam) vn.getHigh(), queue, monitor);
                } else if (vn.getDef() != null) {
                    PcodeOp exploring = vn.getDef();
                    switch (exploring.getOpcode()) {
                        case PcodeOp.COPY:
                        case PcodeOp.CAST:
                            Varnode copied = exploring.getInput(0);
                            if (copied.getDef() != null) {
                                queue.offer(copied);
                            }
                            break;
                        case PcodeOp.MULTIEQUAL:
                            for (Varnode possible : exploring.getInputs()) {
                                if (!visited.contains(possible)) {
                                    queue.offer(possible);
                                }
                            }
                            break;
                        default:
                            sizes.add(Optional.empty());
                            break;
                    }
                } else {
                    sizes.add(Optional.empty());
                }
            }
        }
        return sizes;
    }

    private Set<HighVariable> getRootStackDefinitions(Program program, Varnode node, Register stackRegister,
            TaskMonitor monitor) {
        Set<HighVariable> res = new HashSet<>();
        List<Varnode> visited = new LinkedList<>();
        Queue<Varnode> queue = new LinkedBlockingQueue<>();
        queue.add(node);
        while (!queue.isEmpty()) {
            Varnode curr = queue.poll();
            if (!visited.contains(curr)) {
                HighVariable currHigh = curr.getHigh();
                visited.add(curr);
                if (currHigh != null) {
                    if (currHigh instanceof HighParam) {
                        if (traverse) {
                            resolveThroughFunction(program, (HighParam) currHigh, queue, monitor);
                        }
                        continue;
                    }
                }
                PcodeOp def = curr.getDef();
                if (def != null) {
                    if (def.getOpcode() == PcodeOp.PTRSUB &&
                            stackRegister.contains(program.getRegister(def.getInput(0)))) {
                        List<HighVariable> stackVariables = StreamSupport
                                .stream(((Iterable<HighSymbol>) () -> curr.getHigh().getHighFunction()
                                        .getLocalSymbolMap().getSymbols()).spliterator(), false)
                                .map(HighSymbol::getHighVariable).filter(Objects::nonNull)
                                .filter(hv -> hv.getSymbol().getStorage().isStackStorage())
                                .sorted(Comparator.comparingInt(hv -> hv.getSymbol().getStorage().getStackOffset()))
                                .distinct().collect(Collectors.toList());
                        stackVariables.stream().filter(hv -> hv.getSymbol().getStorage().getStackOffset() ==
                                def.getInput(1).getOffset()).findAny().ifPresent(res::add);
                    } else {
                        queue.addAll(Arrays.asList(curr.getDef().getInputs()));
                    }
                }
            }
        }
        return res;
    }

    private void resolveThroughFunction(Program program, HighParam currHigh, Queue<Varnode> queue,
            TaskMonitor monitor) {
        int slot = currHigh.getSlot();
        Function function = currHigh.getHighFunction().getFunction();
        Set<Function> callers = function.getCallingFunctions(monitor);
        while (callers.stream().anyMatch(Function::isThunk)) { // no thunks, please
            callers = callers.stream().flatMap(f -> {
                if (f.isThunk()) {
                    return f.getCallingFunctions(monitor).stream();
                }
                return Stream.of(f);
            }).collect(Collectors.toSet());
        }
        Map<HighFunction, List<PcodeOp>> dCallers = this.decompileFunctions(callers, monitor);
        if (dCallers == null) {
            return;
        }
        dCallers.values().stream().flatMap(Collection::stream).filter(op -> op.getOpcode() == PcodeOp.CALL)
                .filter(op -> {
                    List<Address> possible = new LinkedList<>();
                    Function f = program.getFunctionManager().getFunctionContaining(op.getInput(0).getAddress());
                    possible.add(f.getEntryPoint());
                    while (f.isThunk()) {
                        f = f.getThunkedFunction(false);
                        possible.add(f.getEntryPoint());
                    }
                    return possible.contains(function.getEntryPoint());
                }).map(op -> op.getInput(slot + 1)).filter(Objects::nonNull).forEach(queue::add);
    }

    private Boolean isStack(HighVariable var) {
        // TODO make isStack trace definition to determine if variable is on stack
        // the purpose of this function is to determine if a target being overflowed is on the stack or not
        // because, well, you can't stack smash a variable not on the stack
        // the current solution is a naive approach based on if the type is a pointer or not
        // to implement this well we will probably need to trace the definition back
        return !(var.getDataType() instanceof PointerDataType);
    }

    @Value
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private static class SmashInfo {

        StackFrame frame;
        Variable smashed;
        int smashSize;
        int offset;
        int end;

        public static SmashInfo get(HighVariable target) {
            StackFrame frame = target.getHighFunction().getFunction().getStackFrame();
            Variable smashed = frame
                    .getVariableContaining(target.getSymbol().getStorage().getStackOffset()); // maybe null?
            if (smashed == null) {
                return null;
            }
            int smashSize = -target.getSymbol().getStorage().getStackOffset() + frame.getReturnAddressOffset();
            int offset;
            if (smashed.getStackOffset() < target.getSymbol().getStorage().getStackOffset()) {
                offset = target.getSymbol().getStorage().getStackOffset() - smashed.getStackOffset();
            } else {
                offset = 0;
            }
            int end = smashed.getDataType().getLength() - offset;
            return new SmashInfo(frame, smashed, smashSize, offset, end);
        }
    }

    @Value
    private static class SmashResult {
        Function smasher;
        Address where;
        Variable smashed;
        String varStatement;
        String instStatement;
        int lengthToReturn;
        boolean complete;
        boolean logged;
        boolean overrun;
    }

}
