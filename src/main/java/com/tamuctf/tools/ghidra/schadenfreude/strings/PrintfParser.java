package com.tamuctf.tools.ghidra.schadenfreude.strings;

import com.google.common.base.Strings;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.FormatString;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.LengthSpecifier;
import com.tamuctf.tools.ghidra.schadenfreude.strings.format.PrintfEntry;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.OptionalLong;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@UtilityClass
public class PrintfParser {
    private final Pattern VALID = Pattern.compile(
            "^(?:(%)(?:(?:([0-9]+)\\$)?)(?:(?:\\*([0-9]+)\\$)?)([0-9]*)((?:hh|h|l|ll|j|z|t|L)?)([iduoxnfegacsp])|(?:[^%]|%%)*)*$");
    private final Pattern PATTERN = Pattern.compile(
            "(?:(%)(?:(?:([0-9]+)\\$)?)(?:(?:\\*([0-9]+)\\$)?)([0-9]*)((?:hh|h|l|ll|j|z|t|L)?)([iduoxnfegacsp])|(?:[^%]|%%)*)");

    public @NotNull PrintfParser.PrintfFormatString parseFormatString(
            @NotNull CharSequence format) throws IllegalArgumentException {
        if (VALID.matcher(format).matches()) {
            PrintfEntry[] printfEntries = PATTERN.matcher(format).results().map(PrintfParser::fromMatch)
                    .toArray(PrintfEntry[]::new);
            return new PrintfParser.PrintfFormatString(format, printfEntries);
        }
        throw new IllegalArgumentException("Invalid printf format string.");
    }

    private PrintfEntry fromMatch(MatchResult result) throws IllegalArgumentException {
        if (result.group(1) == null) {
            String actual = result.group().replaceAll("%%", "%");
            return new PrintfEntry(OptionalLong.empty(), OptionalLong.empty(), OptionalLong.of(actual.length()),
                    null, PrintfEntry.Type.RAW, actual);
        } else {
            PrintfEntry.Type type = PrintfEntry.Type.getByRepresentative(result.group(6).charAt(0));
            OptionalLong argument;
            if (Strings.isNullOrEmpty(result.group(2))) {
                argument = OptionalLong.empty();
            } else {
                argument = OptionalLong.of(Long.parseLong(result.group(2)));
            }
            OptionalLong precision;
            if (Strings.isNullOrEmpty(result.group(3))) {
                precision = OptionalLong.empty();
            } else {
                precision = OptionalLong.of(Long.parseLong(result.group(3)));
            }
            OptionalLong width;
            if (Strings.isNullOrEmpty(result.group(4))) {
                width = OptionalLong.empty();
            } else {
                width = OptionalLong.of(Long.parseLong(result.group(4)));
            }
            LengthSpecifier length = type.getLengthSpecifier(result.group(5));
            return new PrintfEntry(argument, precision, width, length, type, null);
        }
    }

    @SuppressWarnings("RedundantModifiersUtilityClassLombok")
    public static class PrintfFormatString extends FormatString<PrintfEntry, PrintfEntry.Type> {
        public PrintfFormatString(CharSequence unprocessed, PrintfEntry[] entries) {
            super(unprocessed, entries);
        }
    }

}
