package com.tamuctf.tools.ghidra.schadenfreude.strings.format;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
public enum LengthSpecifier {
    CHAR,
    SHORT,
    INT,
    FLOAT,
    PTR,
    DOUBLE,
    WCHAR_T,
    LONG,
    LONGLONG,
    INTMAX_T,
    SIZE_T,
    PTRDIFF_T,
    LONGDOUBLE
}
