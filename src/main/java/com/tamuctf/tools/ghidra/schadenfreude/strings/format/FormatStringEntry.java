package com.tamuctf.tools.ghidra.schadenfreude.strings.format;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.OptionalLong;

public interface FormatStringEntry<T> {
    /**
     * Get the width of the string as known by the user or empty if dynamic.
     *
     * @return The width of the string as known by the user or empty if dynamic.
     */
    @NotNull OptionalLong getWidth();

    /**
     * Get the length of the string in memory or null if unbounded.
     *
     * @return The length of the string in memory or null if unbounded.
     */
    @NotNull LengthSpecifier getLength();

    /**
     * Get the type of format specifier.
     *
     * @return The type of format specifier.
     */
    @NotNull T getType();

    /**
     * Get the content of this entry.
     *
     * @return The raw content of this entry, or null if it is determined at runtime.
     */
    @Nullable String getContent();
}
