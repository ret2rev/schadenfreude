package com.tamuctf.tools.ghidra.schadenfreude.util;

import ghidra.program.model.lang.Register;
import ghidra.program.model.listing.Program;
import ghidra.program.util.ProgramContextImpl;
import ghidra.program.util.VarnodeContext;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@UtilityClass
public class ProgramUtils {

    public Register getStackRegister(@NotNull Program program) {
        VarnodeContext context = new VarnodeContext(program, new ProgramContextImpl(program.getLanguage()),
                new ProgramContextImpl(program.getLanguage()));
        return context.getStackRegister();
    }

}
